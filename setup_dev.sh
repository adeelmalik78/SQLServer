LIQUIBASE_COMMAND_URL=jdbc:sqlserver://demo-db1-win.datical.net:1433;databaseName=NPT_DEV
LIQUIBASE_COMMAND_USERNAME=DATICAL_USER
LIQUIBASE_COMMAND_PASSWORD=DATICAL_USER_PW

liquibase --diffTypes="tables,functions,views,columns,indexes,foreignkeys,primarykeys,uniqueconstraints,data,storedprocedures,triggers,sequences" --dataOutputDirectory=data --changelog-file=StructureWithData.mssql.sql generate-changelog
