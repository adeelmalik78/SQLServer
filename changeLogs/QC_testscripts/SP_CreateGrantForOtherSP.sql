
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Case_C_CaseMoveToOffline_test]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Case_C_CaseMoveToOffline_test]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/**************************************************************************************************************  
* Name		: Case_C_CaseMoveToOffline
* Purpose	: To copy case data from NewCaseArchive DB (SOURCE) and insert into NewCaseOffline Db (DESTINATION)
* Parameters: @gCaseID GUID  
**************************************************************************************************************  
* Change History  
* Date		Author				Comments  
* -------	------				-----------------------------------------------------------------------------------  
* 07Jul13	Pushpendra N		SP created.  
* 12Apr17	Pushpendra N		NC17.2 - Added gReplicationId to H_CasenoteDoc
* 17Apr17	Pushpendra N		NC17.2 - Added gReplicationId to H_CasenoteDoc; updated column sequence in CaseNoteDoclink
* 21Apr17	Pushpendra N		NC17.2 - Changed rollback condition to capture errors in log table
* 02May17	Pushpendra N		NC17.2 - Updated rollback condition to capture errors in log table
* 11May17	Pushpendra N		HPQC#135960 - Existing bug fix - Updated to delete members always then inserte  
* 26May17	Pushpendra N		NC17.2 - Removed openrowset feature  
**************************************************************************************************************/  
CREATE PROCEDURE [dbo].[Case_C_CaseMoveToOffline_test]
(
	@gCaseID	GUID
)
AS
SET NOCOUNT ON
DECLARE @gMemberID GUID
DECLARE @vSQL NVARCHAR(MAX)
DECLARE @bSucess BIT
DECLARE @tableName NVARCHAR(100)
DECLARE @iRet int 
DECLARE @nvchExceptionMessage NVARCHAR (4000) 

CREATE TABLE #Tmp (SNO Int)
CREATE TABLE ##G_Tmp (SNO Int)

SELECT * INTO #CaseDetail FROM dbo.CaseDetail WHERE 1 = 2
SELECT * INTO #CaseContact FROM dbo.CaseContact WHERE 1 = 2


DROP TABLE ##G_Tmp
DROp TABLE #Tmp
DROP TABLE #CaseDetail 
DROP TABLE #CaseContact 
GO
GRANT EXECUTE ON [dbo].[Case_C_CaseMoveToOffline] TO [db_newcase]
GO
