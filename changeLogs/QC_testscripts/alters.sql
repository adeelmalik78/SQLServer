--liquibase formatted sql

--changeset amalik:multiple_alters_bad
ALTER LOGIN Newcase DISABLE

ALTER TABLE dbo.sales
   ADD COUNTRY varchar(50);

ALTER TABLE dbo.sales
   ADD COUNTRY varchar(50);

--changeset amalik:multiple_alters_good

ALTER TABLE dbo.#sales
   ADD COUNTRY varchar(50);

ALTER TABLE dbo.#sales
   ADD COUNTRY varchar(50);


--changeset amalik:sales1_good
CREATE TABLE dbo.sales1 (
   ID int NOT NULL PRIMARY KEY,
   NAME varchar(20),
   REGION varchar(20),
   MARKET varchar(20)
)

--changeset amalik:sales1_bad
CREATE TABLE dbo.sales1 (
   ID int NOT NULL,
   NAME varchar(20),
   REGION varchar(20),
   MARKET varchar(20)
)

--changeset amalik:sales1temp_good
CREATE TABLE dbo.#sales1 (
   NAME varchar(20),
   REGION varchar(20),
   MARKET varchar(20)
)