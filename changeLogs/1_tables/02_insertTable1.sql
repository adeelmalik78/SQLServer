--liquibase formatted sql

--changeset amalik:DB-1021 
--precondition-sql-check expectedResult:0 SELECT COUNT(*) FROM dbo.employee
INSERT INTO [dbo].[employee] (id, name, address1, address2, city, country) VALUES(1, 'Nathan', '5 State St.', '', 'Minneapolis', 'MN');
INSERT INTO DEV.dbo.employee (id, name, address1, address2, city, country) VALUES(2, 'Adeel', '201 Park Ave.', '', 'New York', 'NY');
INSERT INTO DEV.dbo.employee (id, name, address1, address2, city, country) VALUES(3, 'Annette', '85 Lincoln Blvd.', '', 'Austin', 'TX');
INSERT INTO DEV.dbo.employee (id, name, address1, address2, city, country) VALUES(4, 'Lelsey', '8981 Commonwealth Ave.', '', 'Boston', 'MA');
--rollback TRUNCATE TABLE [dbo].[employee];

--changeset amalik:DB-1031
--precondition-sql-check expectedResult:1 SELECT COUNT(*) FROM dbo.sales
INSERT INTO dbo.sales (ID, NAME, REGION, MARKET) VALUES (1,'A', 'B', 'C'); 
INSERT INTO dbo.sales (ID, NAME, REGION, MARKET) VALUES (2,'A', 'B', 'C'); 
INSERT INTO dbo.sales (ID, NAME, REGION, MARKET) VALUES (3,'A', 'B', 'C'); 
INSERT INTO dbo.sales (ID, NAME, REGION, MARKET) VALUES (4,'A', 'B', 'C'); 
INSERT INTO dbo.sales (ID, NAME, REGION, MARKET) VALUES (5,'A', 'B', 'C', 'D'); 
--rollback TRUNCATE table dbo.sales ;


