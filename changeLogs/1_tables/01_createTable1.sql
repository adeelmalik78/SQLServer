--liquibase formatted sql

--changeset amalik:sales context:"UAT"
CREATE TABLE dbo.sales (
   ID int NOT NULL PRIMARY KEY,
   NAME varchar(20),
   REGION varchar(20),
   MARKET varchar(20)
)
--rollback drop table dbo.sales

--changeset amalik:sales2 context:"ALL" labels:"release11,JIRA-1011"
CREATE TABLE dbo.sales2 (
   ID int NOT NULL,
   NAME varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
   REGION varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
   MARKET varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
--rollback drop table dbo.sales2

--changeset amalik:Mod_Information context:"ALL" labels:"release11"
CREATE TABLE dbo.[Initiatives] (
   ID int NOT NULL,
   NAME varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
   REGION varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
   MARKET varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,

)
--rollback drop table dbo.[Initiatives]

--changeset amalik:alter_system context:"UAT"
ALTER TABLE dbo.sales
   ADD COUNTRY varchar(50);
--rollback ALTER TABLE dbo.sales DROP COLUMN COUNTRY


--changeset amalik:alter_sales context:"UAT"
ALTER TABLE dbo.sales
   ADD ZIPCODE varchar(10);
--rollback ALTER TABLE dbo.sales DROP COLUMN ZIPCODE
