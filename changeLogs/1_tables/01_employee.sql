--liquibase formatted sql

--changeset nvoxland:DB-1011
CREATE TABLE [dbo].[employee] (
  id int primary key,
  name varchar(50) not null,
  address1 varchar(50),
  address2 varchar(50),
  city varchar(30)
)
--rollback drop table [dbo].[employee]

--changeset nvoxland:DB-1012
ALTER TABLE [dbo].[employee]
    ADD country varchar(50)
--rollback ALTER TABLE [dbo].[employee] DROP COLUMN country
