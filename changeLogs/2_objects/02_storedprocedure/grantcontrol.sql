--liquibase formatted sql

--changeset amalik:grant_control
GRANT CONTROL ON dbo::CustOrderHist
    TO appUser;  
GO  

--changeset amalik:grant_execute
GRANT EXECUTE ON dbo::CustOrderHist
    TO appUser;  
GO  

--changeset amalik:grant_showplan
GRANT SHOWPLAN ON dbo::CustOrderHist
    TO appUser;  
GO  

--changeset amalik:grant_createview
GRANT CREATE VIEW ON dbo::CustOrderHist
    TO appUser;  
GO  
