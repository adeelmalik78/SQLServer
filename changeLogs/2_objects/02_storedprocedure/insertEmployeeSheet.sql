--liquibase formatted sql
--changeset Sundar:InsertEmployeeTimeSheet-Cycle3  labels:"release16.0,SecurtyRelease" runOnChange:true endDelimiter:GO splitStatements:true 
--ObjectName: InsertEmployeeTimeSheet 
USE [DEV]
GO
/****** Object:  StoredProcedure [dbo].[InsertEmployeeTimeSheet]    Script Date: 2/27/2023 2:36:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertEmployeeTimeSheet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[InsertEmployeeTimeSheet]
GO

/****** Object:  StoredProcedure [dbo].[InsertEmployeeTimeSheet]    Script Date: 2/27/2023 2:36:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Kamraj Sunani
-- Create date: 22-Feb-2013
-- Description:	Insert Employee Timesheet details
-- =============================================

-- =============================================
-- Author:			Kalpana Gupta
-- Modified date:	29-Jan-2014
-- Description:		Included iLeaveAdjustment field
-- =============================================
-- =============================================
-- Author:			Sundar
-- Modified date:	28-Dec-2021
-- Description:		Included timesheet exist logic
-- =============================================
-- =============================================
-- Author:			Sundar
-- Modified date:	30-May-2022
-- Description:		Added iQuarantineDays column for Oryx-177
-- =============================================
-- =============================================
-- Author:			Thanuja P
-- Modified date:	09-Feb-2023
-- Description:		Added nSiteId in the condition of checking the existing timesheets.
-- =============================================
-- =============================================
-- Author:			Thanuja P
-- Modified date:	24-Feb-2023
-- Description:		Updated message and added condition to confirm already Timesheets exist and to create new one.
-- =============================================

CREATE PROCEDURE [dbo].[InsertEmployeeTimeSheet] 
	@ContractId NUMERIC(18, 0),
	@SiteId NUMERIC(18, 0),
	@Month NCHAR(3),
	@Year INT,
	@EmployeeId NUMERIC(18, 0),
	@PositionId NUMERIC(18, 0),
	@OnDays INT,
	@OffDays INT,
	@TravelDays INT,
	@SickDays INT,
	@TrainingDays INT,
	@OtherDays INT,
	@QuarantineDays INT,
	@CreatedBy NVARCHAR(100),
	@CreatedDate DATETIME, 
	@TimeSheetId NUMERIC(18,0) OUTPUT,
	@LeaveAdjustment INT,
	@Message NVARCHAR(300) OUTPUT,
	@ConfirmFlag BIT =0
AS
BEGIN
SET NOCOUNT ON
	DECLARE @nTimeSheetId INT;
	set @Message='';
 IF NOT EXISTS(SELECT nTimeSheetId FROM EmployeeTimeSheet WHERE nchMonth=@Month and iYear=@Year and nEmployeeId=@EmployeeId and nSiteId= @SiteId)
	BEGIN
	IF (EXISTS(SELECT nTimeSheetId FROM EmployeeTimeSheet WHERE nchMonth=@Month and iYear=@Year and nEmployeeId=@EmployeeId) and @ConfirmFlag=0)
	BEGIN
		SET @TimeSheetId =-2;
	END
	ELSE
	BEGIN
	EXEC [UpdateMasterApplicationManagers]'EmployeeTimeSheet',@CreatedBy,@CreatedDate, @OutConfigValue = @nTimeSheetId output
	INSERT INTO EmployeeTimeSheet
		(nTimeSheetId,
		 nContractId,
		 nSiteId,
		 nchMonth,
		 iYear,
		 nEmployeeId,
		 nPositionId,
		 iOnDays,
		 iOffDays,
		 iTravelDays ,
		 iSickDays,
		 iTrainingDays,
		 iOtherDays,
		 iQuarantineDays,
		 nvchCreatedby,
		 dtCreatedDate,
		 iLeaveAdjustment)
	 VALUES
		 (@nTimeSheetId,
		  @ContractId,
		  @SiteId ,
		  @Month, 
		  @Year,
		  @EmployeeId,
		  @PositionId,
		  @OnDays,
		  @OffDays,
		  @TravelDays,
		  @SickDays,
		  @TrainingDays,
		  @OtherDays,
		  @QuarantineDays,
		  @CreatedBy,
		  @CreatedDate,
		  @LeaveAdjustment)
	SET @TimeSheetId = @nTimeSheetId
	END
END
ELSE
	BEGIN
		SET @TimeSheetId = -1;
		SET @Message='A timesheet has already been created for this employee for this Site and period. Please use the ‘Search Employee timesheet’ to retrieve the information.';
	END
END
GO


-- GRANT EXECUTE ON [dbo].[InsertEmployeeTimeSheet] TO [msos_executor] AS [dbo]
-- GO